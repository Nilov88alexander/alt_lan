#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def get_paginator(qwry_list, page_id, page_num):
    paginator = Paginator(qwry_list, page_num)

    try:
        page_context = paginator.page(page_id)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        page_context = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        page_context = paginator.page(paginator.num_pages)
    return page_context


def get_context_single_and_circular_order_by_event_date(model_name, page_id=1):
    '''
    Функция применяется для запроса к моделям имеющим поле "event_date"
    Функция возвращени запись, предыдущую запись и следующую запись.
    В случае отсутствия предыдущей записи вместо нее возвращает последнюю
    В случае отсутствия следующей записи вместо нее возвращает первую
    '''
    page_id = int(page_id) or 1
    context_list = model_name.order_by('event_date')
    context_single = context_list.filter(id=page_id)

    try:
        context_next = context_single.get().get_next_by_event_date()
    except:
        context_next = context_list.first() # get(id = 1)
    try:
        context_previous = context_single.get().get_previous_by_event_date()

    except:
        context_previous = context_list.last()
    return context_single, context_next, context_previous

def get_context_single_and_circular_order_by_order(model_name, page_id=1):
    '''
    Функция применяется для запроса к моделям имеющимполе "order"
    Функция возвращени запись, предыдущую запись и следующую запись.
    В случае отсутствия предыдущей записи вместо нее возвращает последнюю
    В случае отсутствия следующей записи вместо нее возвращает первую
    '''
    page_id = int(page_id) or 1
    context_list = model_name.filter(id__range=(page_id - 3, page_id + 3))
    context_single = context_list.filter(id=page_id)
    order_dict = context_single.values('order',)[0]
    try:
        context_next = context_list.values("id", 'name').get(order=order_dict['order'] + 1)
    except:
        context_next = model_name.first()
    try:
        context_previous = context_list.values("id", 'name').get(order=order_dict['order'] - 1)
    except:
        context_previous = model_name.last()  # reverse()[0:1].get()
    return context_single, context_next, context_previous

