# -*- coding: utf-8 -*-


from django.conf.urls import patterns, include, url
from django.contrib import admin
from AltLan import views
from django.conf import settings
from django.conf.urls.static import static
#from news.rssview import rss_from_url

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'AltLan.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),
                       #url(r'^rss_news$', rss_from_url, name='rss_news'),
                       url(r'^$', views.IndexView.as_view(), name='index'),
                       url(r'^industry_news/((?P<page_id>\d+)/)?$', views.AllRssNewsView.as_view(),
                           name='industry_news'),
                       url(r'^industry_news/category/((?P<category_id>\d+)/)((?P<page_id>\d+)/)?$', views.RssNewsByCategoryView.as_view(),
                           name='industry_news/category/'),
                       url(r'^industry_news_item/((?P<page_id>\d+)/)?$', views.SingleRssNewsView.as_view(), name='industry_news_item'),
                       url(r'^projects/((?P<page_id>\d+)/)?$', views.AllProjectView.as_view(), name='projects'),
                       url(r'^project/((?P<page_id>\d+)/)?$', views.SingleProjectView.as_view(), name='project'),
                       url(r'^products/((?P<page_id>\d+)/)?$', views.AllProductView.as_view(), name='products'),
                       url(r'^product/((?P<page_id>\d+)/)?$', views.SingleProductView.as_view(), name='product'),
                       url(r'^partners/((?P<page_id>\d+)/)?$', views.AllPartnerView.as_view(), name='partners'),
                       url(r'^partner/((?P<page_id>\d+)/)?$', views.SinglePartnerView.as_view(), name='partner'),
                       url(r'^company_news/((?P<page_id>\d+)/)?$', views.AllCompanyNewsView.as_view(), name='company_news'),
                       # include('news.urls', namespace ='news')),
                       url(r'^company_news_item/((?P<page_id>\d+)/)?$', views.SingleCompanyNewsView.as_view(), name='company_news_item'),
                       #include('news.urls', namespace ='news')),
                       url(r'^about/$', views.AboutView.as_view(), name='about'),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^tinymce/', include('tinymce.urls')),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
