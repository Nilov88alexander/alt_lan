#!/usr/bin/python
# -*- coding: utf-8 -*-

from math import ceil

from django.http import request
from django.views.generic import TemplateView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger, Page

# Import views
from company_info.views import PartnerView, TeamView, ContactInfoView
from site_components.views import HeaderFooterMixin, SiteDivisionMixin, SliderView
from news.views import RssNewsView, CompanyNewsView


from services.views import MainProductsView, MainProjectsView, ProjectView, ProductView,\
                           ProductParagraphView, ProjectParagraphView

class IndexView(PartnerView, TeamView, RssNewsView, SliderView,
                CompanyNewsView, MainProductsView, MainProjectsView,
                ContactInfoView, HeaderFooterMixin, TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        self.get_header_footer_context()
        self.get_slides_context()
        self.get_projects_context()
        self.get_rss_news_context_main()
        self.get_company_news_context_main()
        self.get_products_context()
        self.get_partners_context_main()
        self.get_team_context_main()
        self.get_team_text()
        return self.context


class SingleRssNewsView(RssNewsView, HeaderFooterMixin, TemplateView):
    template_name = 'industry_news_item.html'

    def get_context_data(self, page_id=1, **kwargs):
        self.get_header_footer_context()
        self.get_rss_news_context_single(page_id)
        return self.context


class RssNewsByCategoryView(RssNewsView, HeaderFooterMixin, TemplateView):
    template_name = 'industry_news.html'

    def get_context_data(self, category_id=1, page_id=1, **kwargs):
        category_id = category_id or 1
        page_id =  page_id or 1
        context = {'page_name': u'Новости индустрии',
                   'page_url':'/industry_news/category/{}/'.format(category_id)}
        self.context.update(context)

        self.get_header_footer_context()
        self.get_rss_news_context_by_categories(page_id, pagination=10, category_id=category_id)

        return self.context


class AllRssNewsView(RssNewsView, HeaderFooterMixin, SiteDivisionMixin, TemplateView):
    template_name = 'industry_news.html'

    def get_context_data(self, page_id=1, **kwargs):
        mark = 'industry_news'

        context = {'page_name': u'Новости индустрии',
                   'page_url':'/industry_news/'}
        self.context.update(context)

        self.get_division_context(mark)
        self.get_header_footer_context()
        self.get_rss_news_context_all(page_id, pagination=10)
        return self.context

class AllCompanyNewsView(CompanyNewsView, HeaderFooterMixin, SiteDivisionMixin, TemplateView):
    template_name = 'company_news.html'

    def get_context_data(self, page_id):
        page_id =  page_id or 1

        mark = 'company_news'
        context = {'page_name': u'Новости компании',
                   'page_url':'/company_news/'}
        self.get_division_context(mark)
        self.get_header_footer_context()
        self.get_company_news_context_all(page_id=page_id, pagination=10)
        self.context.update(context)
        return self.context


class SingleCompanyNewsView(CompanyNewsView, HeaderFooterMixin, TemplateView):
    template_name = 'company_news_item.html'

    def get_context_data(self, page_id=1, **kwargs):
        self.get_header_footer_context()
        self.get_company_news_context_single(page_id)
        context = {'page_name':u'Новость компании',
                   'page_url':'/company_news_item/'}
        self.context.update(context)

        return self.context


class AllPartnerView(PartnerView, HeaderFooterMixin, SiteDivisionMixin, TemplateView):
    template_name = 'partners.html'

    def get_context_data(self, page_id=1, **kwargs):
        mark = 'partners'
        context = {'page_name': u'Наши партнеры',
                   'page_url':'/partners/'}
        self.context.update(context)

        self.get_division_context(mark)
        self.get_header_footer_context()
        self.get_partners_context_all(page_id, pagination=10)
        return self.context


class SinglePartnerView(PartnerView, HeaderFooterMixin, TemplateView):
    template_name = 'partner.html'

    def get_context_data(self, page_id=1, **kwargs):
        page_id = page_id or 1

        context = {'page_name': u'Партнер компании',
                   'page_url':'/partner/'}
        self.context.update(context)

        self.get_header_footer_context()
        self.get_partners_context_single(page_id)
        return self.context




class AllProjectView(ProjectView, HeaderFooterMixin,SiteDivisionMixin, TemplateView):
    template_name = 'projects.html'

    def get_context_data(self, page_id=1, **kwargs):
        mark = 'projects'
        self.get_division_context(mark)
        context = {'page_name':  u'Проекты компании',
                   'page_url':'/projects/'}
        self.context.update(context)

        self.get_header_footer_context()
        self.get_services_context_all(page_id, pagination=10)
        return self.context

class SingleProjectView(ProjectView,ProjectParagraphView, HeaderFooterMixin, TemplateView):
    template_name = 'single_service.html'

    def get_context_data(self, page_id=1, **kwargs):
        page_id = page_id or 1
        context = {'page_name': u'Проекты компании',
                   'page_url':'/projects/'}
        self.context.update(context)

        self.get_header_footer_context()
        self.get_services_context_single(page_id)
        self.get_paragraphs_context(page_id)

        return self.context


class AllProductView(ProductView, HeaderFooterMixin, SiteDivisionMixin, TemplateView):
    template_name = 'products.html' 

    def get_context_data(self, page_id=1, **kwargs):
        mark = 'products'
        self.get_division_context(mark)
        context = {'page_name': u'Продукты компании',
                   'page_url':'/projects/'}
        self.context.update(context)

        self.get_header_footer_context()
        self.get_services_context_all(page_id, pagination=10)
        return self.context


class SingleProductView(ProductView, ProductParagraphView, HeaderFooterMixin, TemplateView):
    template_name = 'single_service.html'

    def get_context_data(self, page_id=1, **kwargs):
        page_id = page_id or 1
        self.get_header_footer_context()
        context = {'page_name': u'Продукт компании',
                   'page_url':'/product/'}
        self.context.update(context)
        self.get_services_context_single(page_id)
        self.get_paragraphs_context(page_id)
        return self.context


class AboutView(ContactInfoView, TeamView, HeaderFooterMixin, TemplateView):
    template_name = 'about.html'

    def get_context_data(self, **kwargs):
        self.get_header_footer_context()
        context = {'page_name': u'Информация о компании',
                   'page_url':'/about/'}
        self.context.update(context)

        self.get_contact_info()
        self.get_team_context_all()
        return self.context
