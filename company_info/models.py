# coding=utf-8
from django.db import models
from tinymce import models as tinymce_models

# Create your models here.


class Team(models.Model):
    class Meta:
        verbose_name = u'Член команды'
        verbose_name_plural = u"Члены команды"
        ordering = ('order',)

    post = models.CharField(max_length=250, default='', verbose_name=u'Должность')
    name = models.CharField(max_length=250, default='', verbose_name=u'Имя')
    surname = models.CharField(max_length=250, default='', verbose_name=u'Фамилия')
    genetivus_name = models.CharField(max_length=250, default='', verbose_name=u'Фамилия и имя '
                                                                             u'в родительном падеже')
    phone = models.CharField(max_length=250, default='', verbose_name=u'Телефон')
    email = models.EmailField()
    image = models.ImageField(verbose_name=u'Фото', upload_to='photos/team')
    main = models.BooleanField(default=False, verbose_name=u'Вывод на главную страницу')
    order = models.IntegerField(verbose_name=u'Порядковый номер', null=True, default=0, blank=True)

    def __unicode__(self):
        return self.name

class ContactInfo(models.Model):
    class Meta:
        verbose_name = u'Контакт'
        verbose_name_plural = u"Контакты"

    name = models.CharField(max_length=250, default=u'Компания ЗАО «Альт-Лан»', verbose_name=u'Название компании')
    address = models.CharField(max_length=250, default=u'Звездный бульвар\n дом 21\n строение 1\n офис 711',
                               verbose_name=u'Адрес')
    email = models.EmailField(default='info@alt-lan.ru', verbose_name=u'Email адрес')
    phone = models.CharField(max_length=250, default='', verbose_name=u'Телефон')
    # weight =  models.IntegerField(default = 100, verbose_name = 'Порядковый номер' )
    slogan = models.TextField(verbose_name=u"Слоган", blank=True)
    team_text = models.TextField(verbose_name=u"Описание команды", blank=True)

    def __unicode__(self):
        return self.name


class Partner(models.Model):
    class Meta:
        verbose_name = u'Партнер'
        verbose_name_plural = u'Партнеры'
        ordering = ('order',)
    order = models.IntegerField(verbose_name=u'Порядковый номер', null=True, default=0, blank=True)
    name = models.CharField(max_length=250, verbose_name=u'Имя партнера', blank=True)
    text = tinymce_models.HTMLField(verbose_name=u'Описание партнера', blank=True) #models.TextField(verbose_name=u'Описание партнера', blank=True)
    preview = models.CharField(max_length=385,verbose_name=u'Анонс описания партнера. Длина: не более 385 символов', blank=True)
    url = models.CharField(max_length=250, verbose_name=u'Ссылка на ресурс партнера (пример: www.partner.com)', blank=True)
    phone = models.CharField(max_length=250, default='', verbose_name=u'Телефон', blank=True)
    email = models.EmailField(default='', verbose_name=u'Email адрес', blank=True)
    logo_mono = models.ImageField(verbose_name=u'Монохромное изображение логотипа', upload_to='photos/partner')
    logo_colored = models.ImageField(verbose_name=u"Цветное изображение логотипа", upload_to='photos/partner')
    main = models.BooleanField(default=False, verbose_name=u'Вывод на главную страницу')

    def __unicode__(self):
        return self.name