# coding=utf-8

import models

from AltLan.functions import get_paginator, \
    get_context_single_and_circular_order_by_order

# Create your views here.

class PartnerView(object):

    def get_partners_all(self):
        partners_all = models.Partner.objects.all().order_by('order')
        return partners_all

    def get_partners_context_main(self, amount_limit=9, logo_color_limit = 3):
        # Выборка для главной страницы должна быть ограничена 3 эеэментами
        main_partner = self.get_partners_all().filter(main=True)[:amount_limit]
        # У первыч 3 элементов логотипы отображаются в цвете
        partner_logo_color = main_partner[:logo_color_limit]
        # У последующих шести отображаются монохромно
        partner_logo_mono = main_partner[logo_color_limit:]

        context = {'partner_context': partner_logo_color,
                   'partner_logo_mono_context': partner_logo_mono,}
        self.context.update(context)
        return self.context

    def get_partners_context_all(self,page_id, pagination):
        partner_context = get_paginator(self.get_partners_all(), page_id, pagination)
        context= {'page_context':partner_context,}
        self.context.update(context)

    def get_partners_context_single(self, page_id=1):
        context_single, context_next, context_previous = get_context_single_and_circular_order_by_order(self.get_partners_all(), page_id)

        context = {'page_context':context_single,
                   'next_context':context_next,
                   'previous_context':context_previous}
        self.context.update(context)


class ContactInfoView(object):

    def get_contact_info(self):
        contact_info = models.ContactInfo.objects.all()
        return contact_info

    def get_contact_info_context(self):
        context = {'contact_info': self.get_contact_info().get(pk=1)}
        self.context.update(context)

    def get_team_text(self):
        text_query = self.get_contact_info()
        try:
            text = text_query.values_list('team_text')[0][0]
            # Полученнный текст необходимо разделить на 2 половины по ближайшему к середине текста пробелу
            if ' ' in text:
                text_list = text.split(' ')
                len_text_list = len(text_list)
                text_left = ' '.join(text_list[:len_text_list//2])
                text_right = ' '.join(text_list[len_text_list//2:])
        except:
            text_left =  'Команда всегда на высоте.'
            text_right = 'Команда больших профессионалов'

        context = {"team_text_left":text_left,
                   "team_text_right":text_right}
        self.context.update(context)


class TeamView(object):

    def get_team_all(self):
        team_all = models.Team.objects.all()
        return team_all

    def get_team_context_main(self, amount_limit=5):
         # Выборка для главной страницы должна быть ограничена 6 элэментами
        team = self.get_team_all().filter(main=True)[:amount_limit]
        context = {'team_context': team}
        self.context.update(context)
        return self.context

    def get_team_context_all(self):
        pass