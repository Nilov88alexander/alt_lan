from django.contrib import admin

# Register your models here.
import models

class CompanyNewsAdminInline(admin.StackedInline):
    model = models.CompanyNewsImage
    extra = 1

class CompanyNewsAdmin(admin.ModelAdmin):
    inlines = (CompanyNewsAdminInline, )
    list_display = ('author', 'name', 'event_date', 'keywords')
    list_filter = ('keywords', 'event_date',)
    search_fields = ['name', ]
    list_editable = ('keywords',)
class RssNewsAdmin(admin.ModelAdmin):
    search_fields = ['title', ]
    list_filter = ('category_text', 'event_date',)
    list_display = ('title', 'category_text', 'event_date',)

class CategoryRssNewsAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}
    search_fields = ['name', 'name_displayed']
    list_filter = ('visible', 'subscription', )
    list_display = ('name', 'name_displayed','visible', 'subscription', )
    list_editable = ( 'name_displayed','visible', 'subscription', )


admin.site.register(models.CategoryRssNews, CategoryRssNewsAdmin)

admin.site.register(models.CompanyNews, CompanyNewsAdmin)
admin.site.register(models.RssNews, RssNewsAdmin)