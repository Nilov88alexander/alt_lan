# -*- coding: utf-8 -*-
__author__ = 'nilov'

import datetime

from sqlalchemy import create_engine, MetaData , orm

from django import template
register = template.Library()
import feedparser
from bs4 import BeautifulSoup
from urllib2 import urlopen



class BaseException(Exception):
    message = "An unknown exception occurred."

    def __init__(self, **kwargs):
        self.msg = self.message % kwargs
        super(BaseException, self).__init__(self.msg)

    def __repr__(self):
        return "Message: {}".format(self.msg)

class DBSessionError(BaseException):
    message = "Session to DB %(db_connect)s is not established"

class TableDoesNotExistError(BaseException):
    message = "Table %(tables_name)s does not exist"

class DBConnectedError(BaseException):
    message = "Connection to DB %(db_connect)s is not established"

class SiteNameError(BaseException):
    message = "Connection to %(db_connect)s is not established"

class RssNewsDoesNotExistError(BaseException):
    message = "News context does not exist"

class BrokenLinkError(BaseException):
    message = "Link  %(link)s is broken"



class DBPostgres(object):
    def __init__(self, db_connect='postgresql://localhost:5432/alt_lan_db'):
        self.db_connect = db_connect
        self.db_engine = create_engine(db_connect, echo=True)

    def get_session(self):
        self.session = orm.Session(bind=self.db_engine)

    def get_meta(self):
        try: self.meta = MetaData(bind = self.db_engine, reflect = True)
        except:  raise DBConnectedError(self.db_connect)

    def get_tables_from_db(self, tables_name):
        self.get_meta()
        try: self.DBTables = self.meta.tables[tables_name]
        except: raise TableDoesNotExistError(tables_name)

    def insert(self, new_element, tables_name = "news_rssnews",):
        self.get_tables_from_db(tables_name)
        try:self.session.execute(self.DBTables.insert(),new_element)
        except: pass

    def commit(self):
        try: self.session.commit()
        except:  raise DBSessionError(self.db_connect)

    def close(self):
        try: self.session.close()
        except:  raise DBSessionError(self.db_connect)


class CategoriesRssNews(DBPostgres):

    def get_categories(self):
        categories_query = self.db_engine.execute("select * from news_categoryrssnews")
        categories = {(i['id'],i['name']) for i in categories_query}
        return categories

    def get_categories_names(self):
        return {i[1] for i in self.get_categories()}

    def get_categories_id(self):
        return {i[0] for i in self.get_categories()}


class RssReceiver(object):
    def __init__(self, rss_url):
        self.rss_url = rss_url

    @register.inclusion_tag('Wd5PageApp/rssfeed.djhtml')
    def _get_rss_all_dict(self):
        try: self.rss_all_dict = feedparser.parse(self.rss_url)
        except: raise SiteNameError(self.rss_url)

    @property
    def _get_rss_entries_list(self):

        try: self.rss_all_dict#;  print('self.rss_all_dict already exists')
        except: self._get_rss_all_dict(); print('call self._get_rss_all_dict()')

        try:
            rss_entries_list = self.rss_all_dict['entries']
            return rss_entries_list
        except: raise RssNewsDoesNotExistError()

    def _get_categories_from_db(self):
        self.categories_from_db = CategoriesRssNews().get_categories()

    def _check_categories_names(self, categories_str):
        try: self.categories_from_db
        except: self._get_categories_from_db(); print 'call self._get_categories_from_db()'

        categories_str = categories_str.upper()

        # Необходимо проверять кол-во пунктов в строке categories_str (пункты в строке разделены символом ","):
        if ',' not in categories_str:
            self.categories_names_from_db = [i for i in self.categories_from_db if i[1] == categories_str]
            return self.categories_names_from_db
        else:
            rss_item_category_set = set(categories_str.split(','))
            self.categories_names_from_db = [i for i in self.categories_from_db if i[1] in rss_item_category_set]
            return self.categories_names_from_db


    def _parse_rss_item(self, rss_item):
        #categories_raw = ['техниКа','теЛекоМ В2В','телеком','Информатизация','Интернет','Бизнес','Новости Cnews', 'Софт']
        #categories_from_db = [unicode(i, 'utf-8').upper() for i in categories_raw]

        if self._check_categories_names(rss_item.category):
            published_date_raw = rss_item.updated_parsed
            category_id = self. categories_names_from_db[0][0] # Временная мера до того как М2М не сделаю
            published_date = datetime.datetime(published_date_raw[0], published_date_raw[1], published_date_raw[2],
                                               published_date_raw[3], published_date_raw[4], published_date_raw[5],
                                               published_date_raw[6])
            rss_item_dict = {
                    'title': rss_item.title,
                    'preview': rss_item.summary,
                    'link': rss_item.link,
                    'event_date': published_date,
                    'category_text': rss_item.category,
                    'category_id': category_id}
            return rss_item_dict

    def pull_rss_items_list_map(self, rss_count=100):
        rss_entries_list = self._get_rss_entries_list[:rss_count]
        rss_items_list = map(self._parse_rss_item, rss_entries_list)
        return rss_items_list

    def pull_rss_items_list_yield(self, rss_count=100):
        rss_entries_list = self._get_rss_entries_list[:rss_count]
        for item in rss_entries_list:
            yield self._parse_rss_item(item)

    def get_rss_items_list(self, rss_count=100):
        return self.pull_rss_items_list_map(rss_count)

    def get_rss_items_generator(self, rss_count=100):
        return self.pull_rss_items_list_yield(rss_count)


class NewsItemHTMLReceiver(object):
    def __init__(self, link):
        self.link = link

    def get_html_text(self):
        try:
            html_text = urlopen(str(self.link)).read()
            return html_text
        except: BrokenLinkError(str(self.link))


class NewsItem():

    def __init__(self, rss_item):
        self.news_item = rss_item

    def parse_html_text(self, html_text):
        pass

    def get_news_item_dict(self):
        if self.news_item != None:
            html_text = NewsItemHTMLReceiver(self.news_item['link']).get_html_text()
            news_text = self.parse_html_text(html_text)
            self.news_item.update({'text':news_text})
            return self.news_item


class CNewsNewsItem(NewsItem):

    def parse_html_text(self, html_text):
        soup = BeautifulSoup(html_text).find('div', 'NewsBody _ga1_on_')
        text = str(soup)[:-6]
        teg = '</div>'
        ind_1 = text.rfind(teg)
        news_text = text[ind_1 + len(teg):]
        return news_text


class YaRssNewsItem(NewsItem):

    def parse_html_text(self, html_text):
        news_text = "YA news text"
        return news_text


class RssNews(object):
    def __init__(self, rss_url='http://www.cnews.ru/news.xml'):
        self.rss_receiver = RssReceiver(rss_url)

    def get_news_generator(self):
        news_generator = self.rss_receiver.get_rss_items_generator()
        for item in news_generator:
            news_dict = CNewsNewsItem(item)
            yield news_dict.get_news_item_dict()

    def get_news_list(self):
        rss_news_items_list = self.rss_receiver.get_rss_items_list()
        news_list = []
        for item in rss_news_items_list:
            news_dict = CNewsNewsItem(item).get_news_item_dict()
            news_list.append(news_dict)
        return news_list
    


def main():
    rss_news = RssNews(u'http://www.cnews.ru/news.xml')
    rss_news_generator = rss_news.get_news_generator()
    db = DBPostgres()
    db.get_session()
    for item in rss_news_generator:
        if item:
            db.insert(item)
    db.commit()
    db.close()
    
if __name__ == '__main__':
    main()
    from timeit import Timer

    '''
    db_engine = create_engine('postgresql://localhost:5432/alt_lan_db', echo=True)
    new_element = {
        'title':'title1',
        'category_text':'category_text1',
        'category_id':'2',
        'event_date':datetime.datetime(2015, 8, 24, 16, 11, 45),
        'preview':'preview',
        'text':'text',
        'link':'link1',
        }

    session = orm.Session(bind=db_engine)
    Meta = MetaData(bind = db_engine, reflect = True)
    RssNewsDB = Meta.tables["news_rssnews"]
    new= session.execute(RssNewsDB.insert(),new_element)
    session.commit()
    '''

    #rss_news = RssNews(u'http://www.cnews.ru/news.xml')
    #t = Timer('list(rss_news.get_news_generator())', "from __main__ import rss_news")
    #print t.timeit(10)
    #rss_news_generator = rss_news.get_news_generator()
    #print list(rss_news_generator)[0]
    #elements = rss_news_generator
    #print elements
    #db = DBPostgres()
    #db.get_session()
    #for i in elements:
    #    if i:
    #        db.insert(i)
        #except: print i, "\n\nqwe;mvkamflkmdl'fkgm\n\n\n"
    #db.commit()

    #rss_news_list = rss_news.get_news_list()
    #print rss_news_list



    rss_receiver = ''# RssReceiver(u'http://www.cnews.ru/news.xml')
    #rss_receiver.get_rss_items_list()

    # Take items list test
    rss_items_list_test = ''
    if rss_items_list_test:
        print 'map', rss_receiver.pull_rss_items_list_map()

        print 'cycle_form_side', rss_receiver.pull_rss_items_list_cycle_form_side()

        print 'cycle_my', rss_receiver.pull_rss_items_list_cycle_my()

        print 'yield', list(rss_receiver.pull_rss_items_list_yield())

        print 'generator', list(rss_receiver.pull_rss_items_list_generator())

    # Time test

    time_test = ''
    if time_test:
        rss_receiver._get_rss_all_dict()

        t_map_1 = Timer('rss_receiver.pull_rss_items_list_map(1)', "from __main__ import rss_receiver")
        t_cycle_1 = Timer('rss_receiver.pull_rss_items_list_cycle_form_side(1)', "from __main__ import rss_receiver")
        t_cycle_my_1 = Timer('rss_receiver.pull_rss_items_list_cycle_my(1)', "from __main__ import rss_receiver")
        t_yield_1 = Timer('list(rss_receiver.pull_rss_items_list_yield(1))', "from __main__ import rss_receiver")
        t_generator_1 = Timer('list(rss_receiver.get_rss_items_generator(1))', "from __main__ import rss_receiver")

        t_map_10 = Timer('rss_receiver.pull_rss_items_list_map(10)', "from __main__ import rss_receiver")
        t_cycle_10 = Timer('rss_receiver.pull_rss_items_list_cycle_form_side(10)', "from __main__ import rss_receiver")
        t_cycle_my_10 = Timer('rss_receiver.pull_rss_items_list_cycle_my(10)', "from __main__ import rss_receiver")
        t_yield_10 = Timer('list(rss_receiver.pull_rss_items_list_yield(10))', "from __main__ import rss_receiver")
        t_generator_10 = Timer('list(rss_receiver.get_rss_items_generator(10))', "from __main__ import rss_receiver")

        t_map_100 = Timer('rss_receiver.pull_rss_items_list_map(100)', "from __main__ import rss_receiver")
        t_cycle_100 = Timer('rss_receiver.pull_rss_items_list_cycle_form_side(100)', "from __main__ import rss_receiver")
        t_cycle_my_100 = Timer('rss_receiver.pull_rss_items_list_cycle_my(100)', "from __main__ import rss_receiver")
        t_yield_100 = Timer('list(rss_receiver.pull_rss_items_list_yield(100))', "from __main__ import rss_receiver")
        t_generator_100 = Timer('list(rss_receiver.get_rss_items_generator(100))', "from __main__ import rss_receiver")


        print '1   map: {0} | side_cycle: {1} | my_cycle: {2} | ' \
              'list(yield): {3} | list(generator): {4} |'.format(t_map_1.timeit(1000),
                                                                t_cycle_1.timeit(1000),
                                                                t_cycle_my_1.timeit(1000),
                                                                t_yield_1.timeit(1000),
                                                                t_generator_1.timeit(1000))
        print '10  map: {0} | side_cycle: {1} | my_cycle: {2} | ' \
              'list(yield): {3} | list(generator): {4} |'.format(t_map_10.timeit(1000),
                                                                t_cycle_10.timeit(1000),
                                                                t_cycle_my_10.timeit(1000),
                                                                t_yield_10.timeit(1000),
                                                                t_generator_10.timeit(1000))
        print '100 map: {0} | side_cycle: {1} | my_cycle: {2} | ' \
              'list(yield): {3} | list(generator): {4} |'.format(t_map_100.timeit(1000),
                                                                t_cycle_100.timeit(1000),
                                                                t_cycle_my_100.timeit(1000),
                                                                t_yield_100.timeit(1000),
                                                                t_generator_100.timeit(1000))







'''

    # NOT USED
    def pull_rss_items_list_cycle_form_side(self, rss_count=100):

        rss_entries_list = self._get_rss_entries_list

        rss_items_list = []
        try:
            for item in range(rss_count):

                categories_raw = ['техниКа','теЛекоМ В2В','телеком','Информатизация',
                                  'Интернет','Бизнес','Новости Cnews', 'Софт']
                categories_from_db = [unicode(i, 'utf-8').upper() for i in categories_raw]

                rss_item_category_list = rss_entries_list[item].category.split(',')
                if [i for i in rss_item_category_list if i.upper() in categories_from_db]:

                    published_date_raw = rss_entries_list[item].updated_parsed
                    published = datetime.datetime(published_date_raw[0], published_date_raw[1], published_date_raw[2],
                                                  published_date_raw[3], published_date_raw[4], published_date_raw[5],
                                                  published_date_raw[6])
                    rss_items_list.append({
                        'title': rss_entries_list[item].title,
                        'summary': rss_entries_list[item].summary,
                        'link': rss_entries_list[item].link,
                        'date': published,
                        'category': rss_entries_list[item].category,
                    })
                else:
                    rss_items_list.append('Unknown category: {}'.format(rss_entries_list[item].category.encode('utf-8')))
            return rss_items_list

        except:
            pass

    # NOT USED
    def pull_rss_items_list_cycle_my(self, rss_count=100):

        rss_items_list = []
        rss_entries_list = self._get_rss_entries_list[:rss_count]
        for item in rss_entries_list:
            rss_items_list.append(self._parse_rss_item(item))
        return rss_items_list

    # NOT USED
    def pull_rss_items_list_generator(self, rss_count=100):

        rss_entries_list = self._get_rss_entries_list[:rss_count]
        return (self._parse_rss_item(i) for i in rss_entries_list)



'''

# OLD SCUM CODE

'''
@register.inclusion_tag('Wd5PageApp/rssfeed.djhtml')
def rss_pull_feed(rss_feed_url, posts_to_show=20):
    try:
        rss_feed_dict = feedparser.parse(rss_feed_url)
        if not rss_feed_dict['feed']:
            raise SiteNameError(rss_feed_url)
    except SiteNameError as e:
        return e.value
    rss_items_list = []
    try:
        for i in range(posts_to_show):
            published_date_raw = rss_feed_dict['entries'][i].updated_parsed
            published = datetime.date(published_date_raw[0], published_date_raw[1], published_date_raw[2])
            rss_items_list.append({
                'title': rss_feed_dict['entries'][i].title,
                'summary': (rss_feed_dict['entries'][i].summary),
                'link': rss_feed_dict['entries'][i].link,
                'date': published,
                'category': rss_feed_dict['entries'][i].category,
            })
    except:
        # print 'Мало новостей'
        pass
    return rss_items_list


'''

'''

#[unicode(rss_item_dict['category'], 'utf-8').upper().encode('utf-8') ]
#if [i for i in [rss_item_dict['category']][0].split(',') if i in categories_upp]:
'''




'''
class DB_Connect():
    def __init__(self, database='alt_lan_db', user='alt_lan_user',
                       host='localhost',      password='alt_lan'):
                 self.database = database
                 self.user = user
                 self.host = host
                 self.password =password
    def connect(self):
        try:
            connect = psycopg2.connect(database=self.database,
                                       user =self.user,
                                       host=self.host,
                                       password=self.password)
            return connect
        except:
            print("I am unable to connect to the database")

    def cursor(self):
        connect = self.connect()
        cursor = connect.cursor()
        return cursor
    def connect_close(self):
        connect = self.connect()
        connect.close()


class CategoryRssNews(object):
    def __init__(self,
                 category = CategoryRssNews.objects.filter(subscription = True).values('id', 'name')):
        self.category = category
    def get_category_names(self):
        category_names = [name['name'] for name in self.category.values('name')]
        return category_names
    def get_category_id(self):
        category_id = [id['id'] for id in self.category.values('id')]
        return category_id



    #def __init__(self, categories_from_db = CategoryRssNewsView(),
    #             category_form_rss='',
    #             ):



class RssReceiving(object):
    def __init__(self, news_url = u'http://www.cnews.ru/news.xml',
                 news_count = 20):
        self.news_url = news_url
        self.news_count = news_count

    @register.inclusion_tag('Wd5PageApp/rssfeed.djhtml')
    def rss_pull_feed(news_url, news_count):
        try:
            rss_feed_dict = feedparser.parse(news_url)
            if not rss_feed_dict['feed']:
                raise SiteNameError(news_url)
        except SiteNameError as e:
            return e.value
        rss_items_list = []
        try:
            for i in range(news_count):
                published_date_raw = rss_feed_dict['entries'][i].updated_parsed
                published = datetime.date(published_date_raw[0], published_date_raw[1], published_date_raw[2])
                rss_items_list.append({
                    'title': rss_feed_dict['entries'][i].title,
                    'summary': (rss_feed_dict['entries'][i].summary),
                    'link': rss_feed_dict['entries'][i].link,
                    'date': published,
                    'category': rss_feed_dict['entries'][i].category,
                })
        except:
            # print 'Мало новостей'
            pass
        return rss_items_list



class NewsReceiving(CategoryRssNews, RssReceiving):

    def rss_feed(self):
        self.rss_feed()
    def get_category_names(self):
        self.get_category_names()
    def get_category_id(self):
        self.get_category_id()


    html_doc = urlopen(unicode(i['link'])).read()



class NewsParser(object):
    soup = BeautifulSoup(html_doc).find('div', 'NewsBody _ga1_on_').select('p')
    text = str(soup).replace('</p>, <p>', '').replace('[', '').replace(']', '').replace('Читайте также:', '')
    teg = '<span>...</span></a>'
    ind1 = text.find(teg)
    text = text[ind1 + len(teg):]
    ind2 = text.find(teg)
    text = text[ind2 + len(teg):]

'''


'''
        for i in self.rss_entries_list:
            published_date_raw = i.updated_parsed
            published = datetime.datetime(published_date_raw[0], published_date_raw[1], published_date_raw[2],
                                          published_date_raw[3], published_date_raw[4], published_date_raw[5],
                                          published_date_raw[6])
            self.rss_items_list.append({
                'title': i.title,
                'summary': i.summary,
                'link': i.link,
                'date': published,
                'category': i.category,})
'''




#_____________________________________
'''
def db_connect(database='alt_lan_db', user='alt_lan_user', host='localhost', password='alt_lan'):
    # Проверяем подключение к базе
    try:
        connect = psycopg2.connect(database=database, user=user, host=host, password=password)
        cursor = connect.cursor()
        return connect, cursor
    except:
        print("I am unable to connect to the database")


def catery_list_puller(select="""SELECT id, name from news_categoryrssnews WHERE subscription = 'TRUE'"""):
    # Достаем необходимые катекории, по которым будет производиться загрузка
    connect, cursor = db_connect()
    cursor.execute(select)
    rows = cursor.fetchall()
    connect.close()
    return [(id, name) for id, name in rows]


def cnews_rss_parser(pull_feed_dict):
    connect, cursor = db_connect()
    category_list = catery_list_puller()
    log_context_list = []
    for i in pull_feed_dict:
        for category in category_list:
            try:
                category_name = category[1].decode('utf-8')
            except:
                category_name = category[1]
                # if category_name in i['category']:

                # Радактируем текст статьи
            html_doc = urlopen(unicode(i['link'])).read()

            soup = BeautifulSoup(html_doc).find('div', 'NewsBody _ga1_on_')#.select('p').select('ul')
            text = str(soup)[:-6]#.replace('div', 'xyi') #.replace('</p>, <p>', '').replace('[', '').replace(']', '').replace('Читайте также:', '')
            teg = '</div>'
            ind1 = text.rfind(teg)
            #text = '<> asdasdasd</> asd'
            #reg = re.compile(r"(?<=^|>)[^><]+?(?=<|$)")
            #text = reg.findall(text)

            #teg = '<span>...</span></a>'
            #teg1 = '</div>\n</div>'
            #ind1 = text.find(teg)
            text = text[ind1 + len(teg):]
            #ind2 = text.find(teg1)
            #text = text[ind2 + len(teg):]#.replace('</p>,<p', '</p><p').replace('</p>, <p', '</p><p')[6:-1]
            #ind3 = text.find(teg)
            #text = text[ind3 + len(teg):]

            # Производим запись в базу данных
            query_dict = {'category_id': category[0],
                          'title': i['title'].encode('utf-8'),
                          'preview': i['summary'].encode('utf-8'),
                          'event_date': i['date'],
                          'category_text': i['category'].encode('utf-8'),
                          'link': i['link'],
                          'text': text}
            query = """INSERT INTO news_rssnews(category_id,
            title,
            category_text,
            event_date,
            preview,
            text,link)
            VALUES (%(category_id)s,%(title)s,%(category_text)s,%(event_date)s,%(preview)s,%(text)s,%(link)s)"""
            connect.commit()
            try:
                cursor.execute(query, query_dict)
                log_status = 'Create news: - '
                log_link = i['link']
            except:
                # print 'News already exists: - ', link
                log_status = 'News already exists: - '
                log_link = i['link']
            log_context_list.append((log_status, log_link))
            connect.commit()

    context = {'context_list': log_context_list}
    connect.close()
    return context


def rss_from_url(request):
    try:
        pull_feed_dict = rss_pull_feed(u'http://www.cnews.ru/news.xml', 100)
        if type(pull_feed_dict) is unicode:
            raise SiteNameError(pull_feed_dict)
    except SiteNameError as e:
        log_context_list = ((u'Нет доступа к сайту ',), (u'www.cnews.ru',), (e.value,))
        context = {'context_list': log_context_list}
        return render(request, 'rss_news.html', context)
    context = cnews_rss_parser(pull_feed_dict)
    cnews_rss_parser(pull_feed_dict)
    if request:
        return render(request, 'rss_news.html', context)


if __name__ == '__main__':
    pull_feed_dict = rss_pull_feed(u'http://www.cnews.ru/news.xml', 100)
    cnews_rss_parser(pull_feed_dict)

'''