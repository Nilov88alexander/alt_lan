from django.conf.urls import patterns, url
from views import CompanyNewsListView
urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'AltLan.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),
                       url(r'^news/$', CompanyNewsListView.as_view() ,name='news'),
)
