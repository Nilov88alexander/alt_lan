# -*- coding: utf-8 -*-

import models

from AltLan.functions import get_paginator, \
    get_context_single_and_circular_order_by_event_date


# Create your views here.

class RssNewsView(object):

    def get_rss_news_all(self):
        rss_news_all = models.RssNews.objects.all().order_by("-event_date")
        return rss_news_all

    def get_categories_rss_news(self):
        categories = models.CategoryRssNews.objects.filter(visible=True).values('id', 'name')
        return categories

    def get_single_news(self, page_id):
        try: self.single_news
        except: self.single_news = self.get_rss_news_all().filter(pk=page_id)

    def _get_single_news_category(self):
        category = self.single_news.values_list('category')
        return category

    def _get_single_news_id(self):
        id = self.single_news.values_list('id')
        return id

    def _get_news_exclude_item(self):
        # Метод исключает из выборки ывводимую новость
        page_id = self._get_single_news_id()
        news_exclude_item = self.get_rss_news_all().exclude(pk=page_id).order_by("-event_date")
        return news_exclude_item

    def _get_rss_single_news_category_last_5(self, amount_limit=5):
        single_news_category = self._get_single_news_category()
        # Необходимы 5 последних элементов по категории выводимой новости, исключая выводимую новость
        rss_category_last_5 = self._get_news_exclude_item().filter(category=single_news_category)[:amount_limit]
        return rss_category_last_5

    def _rss_last_5(self, amount_limit= 5):
        # Необходимы 5 последних элементов, исключая новости по категориям выводимой новости и саму выводимую новость
        id_rss_category_last_5 = self._get_rss_single_news_category_last_5().values_list('id')
        rss_last_5 = self._get_news_exclude_item().exclude(pk__in=id_rss_category_last_5)[:amount_limit]
        return rss_last_5

    def get_rss_news_context_main(self, amount_limit= 6):
        # Выборка для главной страницы должна быть ограничена 6 элэментами
        rss_news = self.get_rss_news_all()[:amount_limit]
        len_rss_news = len(rss_news)//2
        rss_news_left = rss_news[:len_rss_news]
        rss_news_right = rss_news[len_rss_news:]

        context = {
            "rss_news_left": rss_news_left,
            "rss_news_right": rss_news_right,
        }
        self.context.update(context)

    def get_rss_news_context_all(self, page_id=1, pagination=10, *args):
        rss_news_all = get_paginator(self.get_rss_news_all(), page_id, pagination)

        categories = self.get_categories_rss_news()
        context = {"page_context": rss_news_all,
                   'categories':categories}
        self.context.update(context)
        
    def get_rss_news_context_single(self, page_id=1):
        self.get_single_news(page_id)
        context = {"single_news": self.single_news,
                   "rss_last_5": self._rss_last_5(),
                   "rss_category_last_5": self._get_rss_single_news_category_last_5()}
        self.context.update(context)

    def get_rss_news_context_by_categories(self, page_id=1, pagination=10, category_id=1):
        rss_news_all = get_paginator(self.get_rss_news_all().filter(category_id=category_id)
                                                                    ,page_id, pagination)
        categories = self.get_categories_rss_news()
        context = {"page_context": rss_news_all,
                   'categories':categories}
        self.context.update(context)




class CompanyNewsView(object):

    def get_company_news_all(self):
        company_news_all = models.CompanyNews.objects.all().order_by("-event_date")
        return company_news_all

    def get_company_news_context_all(self, page_id=1, pagination=10):
        company_news_context = get_paginator(self.get_company_news_all(),page_id, pagination)
        context= {'page_context':company_news_context,}
        self.context.update(context)

    def get_company_news_context_main(self, amount_limit=3):
        company_news_context = self.get_company_news_all()[:amount_limit]
        context= {'company_news_context':company_news_context,}
        self.context.update(context)

    def get_company_news_context_single(self, page_id=1):

        context_single, context_next, context_previous = get_context_single_and_circular_order_by_event_date(self.get_company_news_all(), page_id)
        context_images = context_single.get().news_image.all() or context_single
        image_count = context_images.count() or context_images.image_count()
        context = {'page_context':context_single,
                'next_context':context_next,
                'previous_context':context_previous,
                'context_images':context_images,
                'image_count':image_count}
        self.context.update(context)



