#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.db import models
from tinymce import models as tinymce_models

# Create your models here.


class Product(models.Model):
    class Meta:
        verbose_name = u'Продукт/услуга'
        verbose_name_plural = u'Продукты/услуги'
        ordering = ('order',)

    name = models.CharField(verbose_name=u'Имя продукта/услуги', max_length=250, default='', blank=True)
    preview = models.CharField(verbose_name=u'Анонс продукта/услуги. Длина: не более 385 символов',
                               max_length=385, default='', blank=True, )
    url = models.CharField(verbose_name=u'Относительный путь', max_length=250, blank=True)
    order = models.IntegerField(verbose_name=u'Порядковый номер', null=True, default=0, blank=True)
    keywords = models.CharField(verbose_name=u'Ключевые слова (через пробел)', max_length=250, default='', blank=True)
    slug = models.SlugField()
    image = models.ImageField(null=True,verbose_name=u'Изображение', upload_to='photos/product')

    def __unicode__(self):
        return self.name


class ProductParagraph(models.Model):
    class Meta:
        verbose_name = u'Параграф к продукту/услуге'
        verbose_name_plural = u'Параграфы к продукту/услеге'
        ordering = ('order',)

    product = models.ForeignKey(Product, related_name='paragraph')
    text = tinymce_models.HTMLField(verbose_name=u'Параграф продукта/услуги', blank=True) #models.TextField(verbose_name=u'Параграф продукта/услуги', blank=True)
    order = models.IntegerField(verbose_name=u'Порядковый номер', null=True, default=0, blank=True)
    image = models.ImageField(null=True,blank=True, verbose_name=u'Изображение', upload_to='photos/product/paragraph', default="")
    text_image = models.TextField(verbose_name=u'Описание изображения', blank=True)


class Project(models.Model):
    class Meta:
        verbose_name = u'Проект'
        verbose_name_plural = u'Проекты'
        ordering = ('order',)

    name = models.CharField(verbose_name=u'Имя проекта', max_length=250, default='', blank=True)
    preview =  models.CharField(verbose_name=u'Анонс проекта. Длина: не более 385 символов',
                               max_length=385, default='', blank=True, )
    url = models.CharField(max_length=250, verbose_name=u'Относительный путь', null=True)
    order = models.IntegerField(verbose_name=u'Порядковый номер', null=True, default=0, blank=True)
    keywords = models.CharField(verbose_name=u'Ключевые слова (через пробел)', max_length=250, default='', blank=True)
    slug = models.SlugField()
    image = models.ImageField(null=True, verbose_name=u'Изображение', upload_to='photos/project')

    def __unicode__(self):
        return self.name


class ProjectParagraph(models.Model):
    class Meta:
        verbose_name = u'Параграф к проекту'
        verbose_name_plural = u'Параграфы к проекту'
        ordering = ('order',)

    project = models.ForeignKey(Project, related_name='paragraph')
    text = tinymce_models.HTMLField(verbose_name=u'Параграф проекта', blank=True) #models.TextField(verbose_name=u'Параграф продукта/услуги', blank=True)
    order = models.IntegerField(verbose_name=u'Порядковый номер', default=0, blank=True)
    image = models.ImageField(null=True,blank=True,  verbose_name=u'Изображение', upload_to='photos/project/paragraph', default="")
    text_image =  models.TextField(verbose_name=u'Описание изображения', blank=True)