# -*- coding: utf-8 -*-

import models

from AltLan.functions import get_paginator, \
    get_context_single_and_circular_order_by_order
from math import ceil
# Create your views here.



class Services(object):

    def get_items_all(self):
        # Метод будет переопраделен в наследниках.
        pass

    def get_services_context_all(self, page_id, pagination):
        context = get_paginator(self.get_items_all(), page_id, pagination)
        context= {'page_context':context,}
        self.context.update(context)

    def get_services_context_single(self, page_id):
        context_single, context_next, context_previous = get_context_single_and_circular_order_by_order(self.get_items_all(), page_id)
        context = {'page_context':context_single,
                   'next_context':context_next,
                   'previous_context':context_previous}
        self.context.update(context)

class ProductView(Services):

    def get_items_all(self):
        items_all = models.Product.objects.all().order_by('order')
        return items_all

class ProjectView(Services):

    def get_items_all(self):
        items_all = models.Project.objects.all().order_by('order')
        return items_all


class MainProjectsView(ProjectView):
    # Выборка для главной страницы должна быть ограничена 3 элэментами
    def get_projects_context(self, amount_limit=3):
        context = self.get_items_all()[:amount_limit]
        context= {'product_context':context}
        self.context.update(context)


class MainProductsView(ProductView):
    # Выборка для главной страницы должна быть ограничена 3 элэментами
    def get_products_context(self, amount_limit=3):
        context = self.get_items_all()[:amount_limit]
        context= {'product_context':context}
        self.context.update(context)

class Paragraph(object):

    def get_paragraphs_all(self, page_id):
        # Метод будет переопраделен в наследниках.
        pass

    def get_paragraphs_context(self,page_id):
        context_paragraphs = self.get_paragraphs_all(page_id)
        count_paragraph = ceil(context_paragraphs.count()/2.0)
        context = {'context_col1':context_paragraphs[:count_paragraph],
                   'context_col2':context_paragraphs[count_paragraph:]}
        self.context.update(context)


class ProjectParagraphView(Paragraph):
    def get_paragraphs_all(self, page_id):
        return models.ProjectParagraph.objects.filter(project__id=page_id)

class ProductParagraphView(Paragraph):
    def get_paragraphs_all(self, page_id):
        return models.ProductParagraph.objects.filter(product__id=page_id)
