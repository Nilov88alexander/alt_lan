from django.contrib import admin
import models

# Register your models here.
class MenuAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('url',)}
    list_display = ('name', 'order',)
    list_editable = ('order',)


class SliderAdmin(admin.ModelAdmin):
    list_display = ('title', 'order',)
    list_editable = ('order',)

class SiteDivisionAdmin(admin.ModelAdmin):
    list_display = ('name','menu')
    list_editable = ('menu',)
admin.site.register(models.SiteDivision, SiteDivisionAdmin)
admin.site.register(models.Menu, MenuAdmin)
admin.site.register(models.Slider, SliderAdmin)