# -*- coding: utf-8 -*-

from django.db import models

# Create your models here.
from django.db import models
from tinymce import models as tinymce_models
from sorl.thumbnail import get_thumbnail


class Slider(models.Model):
    """
    docstring for Slider:
    Класс описывает бизнес логику слайдера.
    """
    class Meta:
        verbose_name = u'Элемент слайдера'
        verbose_name_plural = u'Элементы слайдера'
        ordering = ('order',)

    title = models.CharField(verbose_name=u'Заголовок элемента слайдера', max_length=50, blank=True)
    text = tinymce_models.HTMLField(verbose_name=u'Текст элемента слайдера. Длина: не более 200 символов',
                                    max_length=200, blank=True)
    image = models.ImageField(verbose_name=u'Изображение для элемента слайдера', upload_to='photos/slider', blank=True)
    order = models.IntegerField(default=0, verbose_name=u'Порядковый номер')

    def img_url(self):
        return u'%s' % self.image.url

    def preview_image_url(self):
        image_path = get_thumbnail(self.image.url, '60x60') # создается миниатюра
        return image_path


class Menu(models.Model):
    class Meta:
        verbose_name = u'Пункт меню'
        verbose_name_plural = u"Пункты меню"
        ordering = ('order',)
    name = models.CharField(max_length=250, verbose_name=u'Заголовок', blank=True)
    url = models.CharField(max_length=250, verbose_name=u'Ссылка. '
                                                        u'При наличии символа "#" перед названием '
                                                        u'будет вести на главную страницу '
                                                        u'на соотвествующий раздел. '
                                                        u'При его отсутствии ведет на '
                                                        u'соответствующую страницу сайта', blank=True)
    slug = models.SlugField(blank=True)
    order = models.IntegerField(default=0, verbose_name=u'Порядковый номер', blank=True)

class SiteDivision(models.Model):
    class Meta:
        verbose_name = u'Раздел сайта'
        verbose_name_plural = u"Разделы сайта"
    name = models.CharField(verbose_name=u'Заголовок раздела сайта', max_length=50, blank=True)
    mark = models.CharField(verbose_name=u'Марка раздела сайта', max_length=50, blank=True)
    slogan = models.CharField(verbose_name=u'Слоган раздела сайта', max_length=50, blank=True)
    text = tinymce_models.HTMLField(verbose_name=u'Текст раздела сайта. Длина: не более 350 символов', max_length=350, blank=True)
    image = models.ImageField(verbose_name=u'Изображение для раздела сайта', upload_to='photos/site_division', blank=True)
    menu = models.ForeignKey(Menu, related_name='division', blank=True, default=1)