﻿jQuery(document).ready(function ($) {
    setDecoAngles();
    setTabs();
    setImgTransition();    
    responsiveMenu();
    
    $('#menu_items').slicknav();
   
     
    $("body").show().animate({ "opacity": "1" }, 1500);  //fade the first div.section in on page load

    $(window).resize(function () {
        if ($(window).width() > 900) setDecoAngles();
        responsiveMenu();
    });
	
	function responsiveMenu() {
		var liTotalWidth = 0;
		var ulWidth = $('#menu_items').width() - 20;

		$('#menu_items li').each(function () {
			liTotalWidth = liTotalWidth + $(this).outerWidth();
		});

		if (ulWidth <= liTotalWidth) { 			
			//$('.nav_wrapper').css('visibility', 'hidden');
			//$('.slicknav_menu').show();
		} else if (liTotalWidth == 0) {
			return;
		}
		else {
			
			//$('.nav_wrapper').css('visibility', 'visible');
			//$('.slicknav_menu').hide();
		}
	};
		
   function setDecoAngles() {
        var a = $(window).width() + 365,
            b = $(".deco_angle.inner").css("bottom"),
            c = 1850;

        if ($(window).width() < 900) {
            $(".deco_angle.outer").css("border-left-width", c);
            $(".deco_angle.inner").css("border-left-width", c);
        } else {
            $(".deco_angle.outer").css("border-left-width", a);
            $(".deco_angle.inner").css("border-left-width", a);
        }

        $(".back").css("height", b);
    }

    function setImgTransition() {
        $('.fadein').each(function () {
            var img = $(this).find('img')
            var std = img.attr("src");
            var hover = std.replace("mono", "colored");
            img.clone().insertBefore(img).attr('src', hover).attr('class', 'colored')
            $('.colored').css({
                position: 'absolute',
                opacity: 0
            });
            $(this).mouseenter(function () {
                img.stop().fadeTo(600, 0);
                img.prev().fadeTo(400, 1);
            }).mouseleave(function () {
                img.stop().fadeTo(400, 1);
                img.prev().fadeTo(600, 0);
            });
        });
    }    

    function setTabs() {
        var tabs = $('.tabs'),
        controls = tabs.find('.projects_slider_controls li'),
          slides = tabs.find('.projects_slides li');

        controls.first().addClass('current');
        slides.first().addClass('current').fadeIn();
       
        $('a', controls).each(function (i) {
            $(this).attr('data-show_slide', i);
            $($(this).closest('.tabs').find('.projects_slides li')[i]).attr('data-targeted_slide', i);
        });           

        $('a', controls).each(function (i) {
            $(this).on('click', function () {            	
                $(controls).removeClass('current')
                $(this).parent().addClass('current');
                              
                $('.projects_slides li').removeClass('current').hide();
                $('.tabs').find('[data-targeted_slide="' + $(this).attr('data-show_slide') + '"]').fadeIn(1000).addClass('current');
            })
        })
    };
    
  
});//end document.ready


