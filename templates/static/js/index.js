jQuery(document).ready(function($) {
	
	/*start --------------------------------- 
	 * Сценарии для раздела контакты */
	var $loader = $('.loader'), mapHeight = 0;

	if ( typeof ymaps != 'undefined')
		return;
	$.getScript('http://api-maps.yandex.ru/2.1/?lang=ru_RU', function() {
		ymaps.ready(init);

		$loader.css('background-image', 'none').fadeOut(3000);
		var myMap;

		//TODO: найти, как перестраивать высоту карты при ресайзе окна
		mapHeight = $('.contacts').height() * 0.56 - $('.contacts .details').height();

		$('#altlan_map').height(mapHeight);

		function init() {
			myMap = new ymaps.Map("altlan_map", {
				center : [55.811032, 37.624444],
				zoom : 16,
				controls : ["zoomControl", "typeSelector"],
				behaviors : ['drag']
			});

			myBalloon = new ymaps.Placemark([55.811032, 37.624444], {
				balloonContentBody : "<strong>ЗАО «Альт-Лан»</strong> <br/>г. Москва, Звёздный бульвар 21, строение 1, офис 711"
			});
			myMap.geoObjects.add(myBalloon);
			myMap.balloon.open([55.811032, 37.624444], "<strong>ЗАО «Альт-Лан»</strong> <br/>г. Москва, Звёздный бульвар 21, строение 1, офис 711", {
				// Опция: не показываем кнопку закрытия.
				closeButton : false
			});
			
			$(window).bind({
				resize: function(){
			//debugger;
			$('#altlan_map').height(14);
				//методе перерисовки карты
		}
			})
		}
	});
	
	/*end Сценарии для раздела контакты */
	
	$('#fullpage').fullpage({
		anchors : ['home', 'products', 'projects', 'partners', 'contacts'],
		menu : '#menu_items',
		autoScrolling : false,
		resize : false,
		afterLoad : function(anchorLink, index) {
			if (index !== '1') {
				$('.nav_wrapper').addClass('minor');
			};

			//when scrolling to section 1 has ended
			if (index == '1') {
				$('.nav_wrapper').removeClass('minor');
			};
		}
	});
	
	/*start --------------------------------- 
	 * Сценарии для раздела новости отрасли */
	$('.area_news article .intro').readmore({
		moreLink: false,
		lessLink: false,
		embedCSS: false,
		beforeToggle: function(trigger, element, expanded){
			var link = $(element).attr('data-link');
			 	$(element).wrapInner('<a class="read_more" href="' + link + '"/>')
			 	$(element).unbind('click')
		},
		afterToggle: function(trigger, element, expanded){
			 if( expanded) { 
			 	
     			
     		}
		}
	});
	
	$('.area_news article .intro').click(function(){		
		$(this).readmore('toggle');
	});	
	/*end Сценарии для раздела новости отрасли */
	
	var fadeStart = 80, // 100px scroll or less will equiv to 1 opacity
            fadeUntil = 150,// 200px scroll or more will equiv to 0 opacity
            fading = $('.display_slide');

		$(window).bind({
			scroll: function () {
			var offset = $(document).scrollTop(),
                opacity = 0;
			console.log(offset);
			if (offset <= fadeStart) {
				opacity = 1;
			} else if (offset <= fadeUntil) {
				opacity = 1 - offset / fadeUntil;
			}
			fading.css('opacity', opacity);
		}});
		
});
